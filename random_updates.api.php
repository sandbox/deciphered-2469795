<?php

/**
 * @file
 * API documentation for Random updates.
 */

/**
 * Allows modules to execute random update during update.php.
 */
function hook_random_updates() {
  // Pick a random theme.
  $themes       = list_themes(TRUE);
  $random_theme = array_rand($themes);

  // Enable and set theme.
  theme_enable(array($random_theme));
  variable_set('theme_default', $random_theme);
  menu_rebuild();
}
