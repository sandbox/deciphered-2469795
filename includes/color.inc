<?php

/**
 * @file
 * Color module integration.
 */

/**
 * Implements hook_random_updates_info().
 */
function color_random_updates_info() {
  return array(
    'color' => array(
      'weight' => 10,
    ),
  );
}

/**
 * Implements hook_random_updates() on behalf of color.module.
 */
function color_random_updates() {
  $theme = variable_get('theme_default', NULL);
  $info  = color_get_info($theme);

  // Randomise color scheme if available.
  if ($info && isset($info['schemes'])) {
    $scheme     = array_rand($info['schemes']);
    $form       = array();
    $form_state = array(
      'values' => array(
        'info'    => $info,
        'palette' => $info['schemes'][$scheme]['colors'],
        'scheme'  => $scheme,
        'theme'   => $theme,
      )
    );
    color_scheme_form_submit($form, $form_state);

    return t('Color scheme changed to: !scheme', array('!scheme' => $scheme));
  }
}
