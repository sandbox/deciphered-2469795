<?php

/**
 * @file
 * System module integration.
 */

/**
 * Implements hook_random_updates() on behalf of system.module.
 */
function system_random_updates() {
  $messages = array();

  // Pick a random theme.
  $themes       = list_themes(TRUE);
  $random_theme = array_rand($themes);

  // Enable and set theme.
  theme_enable(array($random_theme));
  variable_set('theme_default', $random_theme);
  menu_rebuild();

  $messages[] = t('Theme changed to: !theme', array('!theme' => $random_theme));

  return $messages;
}
