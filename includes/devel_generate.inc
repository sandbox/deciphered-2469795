<?php

/**
 * @file
 * Devel generate module integration.
 */

/**
 * Implements hook_random_updates() on behalf of devel_generate.module.
 */
function devel_generate_random_updates() {
  $messages = array();
  require_once(drupal_get_path('module', 'devel_generate') . '/devel_generate.inc');

  // Randomise site name.
  $site_name = devel_generate_word(rand(6, 12));
  variable_set('site_name', $site_name);

  $messages[] = t('Site name changed to: !site_name', array('!site_name' => $site_name));

  // Create new front page content.
  $node_types = node_type_get_types();
  $type       = array_rand($node_types);

  $form_state = array(
    'values' => array(
      'node_types' => array(
        $type => $type,
      ),
      'num_nodes'  => 1,
    ),
  );
  devel_generate_content($form_state);

  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->propertyOrderBy('nid', 'DESC')
    ->range(0, 1)
    ->execute();
  if (!empty($result['node'])) {
    $nid = key($result['node']);
    variable_set('site_frontpage', "node/{$nid}");

    $messages[] = t('Site frontpage changed to: node/!nid', array('!nid' => $nid));
  }

  return $messages;
}
